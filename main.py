import requests
from bs4 import BeautifulSoup
import pandas as pd

key = input('please enter the term:')

url = 'https://www.hallogsm.com/hp/{}/'.format(key)
headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36'

}

datas = []
#count_page = 0
for page in range(1, 5):
    #count_page+=1
    #print('scraping page :',count_page)
    req = requests.get(url + 'page/' +str(page), headers=headers)
    soup = BeautifulSoup(req.text, 'html.parser')
    items = soup.findAll('div', 'aps-product-box')

    for item in items:
        nama = item.find('h2', 'aps-product-title').text.strip()
        harga = item.find('span', 'aps-price-value').text
        link_image = item.find('div', 'aps-product-thumb').find('img')['data-lazy-src']
        # alt_item = item.find('div', 'aps-product-thumb').find('img')['alt']
        # alt_item = str(alt_item).replace(' ', '-').replace('/', '').replace('*', '') + '.jpg'

        # tambah data dictionary buat menyimpan data
        data_dict = {
            'nama':nama,
            'harga':harga,
            'link gambar':link_image,
        }

        # check item if exist
        print(data_dict)
        datas.append(data_dict)


# membuat csv dengan pandas
print('Creating CSV')
dataframe = pd.DataFrame(datas)

# Creating CSV with pandas
dataframe.to_csv(f"finish/{key}.csv", index=False)


# Read Json